<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\BerandaController;
use App\Http\Controllers\ContactController;
use App\Http\Controllers\HobbyController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/',[App\Http\Controllers\BerandaController:: class, 'index'] );

Route::get('/contac',[App\Http\Controllers\ContactController:: class, 'contac'] );
    
Route::get('/hobby', [App\Http\Controllers\HobbyController:: class, 'hobby'] );
   

